Issue with versioning in the release process:

The following principles should hold:

* The master branch should hold the latest released version (if there is any)
* A develop branch should hold the latest snapshot version

* When preparing a release, a release branch is created with all data and metadata based on the release version
  * This means, that the version has been updated for the release branch
* Upon completion of release-perform, the release branch is merged into master
   subsequently, master is merged into develop and the source config of develop branch has its version incremented to the next snapshot version


The main questions are:

* Do we need to run the effective dcat model goal multiple times?
	what happens if a goal depends on the same goal multiple times?
