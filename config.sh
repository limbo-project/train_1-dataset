# REQUIRED parameters

# The ID of the Limbo dataset
LIMBO_DATASET_ID=train_1-dataset
# The ID of the Limbo dataset
LIMBO_DATASET_NAME="Geodaten Betriebsstellen"
# The Limbo namespace
#LIMBO_NS=https://data.limbo-project.org/
# The graph of the Limbo dataset
#LIMBO_DATASET_GRAPH=$LIMBO_NS$LIMBO_DATASET_ID/
# The source namespace from the lifting portal
#SOURCE_NS=https://portal.limbo-project.org/
# The target namespace
#TARGET_NS=https://data.limbo-project.org/
# The Limbo dataset license
LIMBO_DATASET_LICENSE=NullLicense
# The mcloud namespace
#MCLOUD_NS=https://metadata.limbo-project.org/
# The dataset in the mcloud catalog that the Limbo dataset refers to
#MCLOUD_DATASET=dataset-Stationsdaten-RNI--552695060
# The distribution in the mcloud catalog that the Limbo dataset is based on
MCLOUD_DISTRIBUTION=distribution-Stationsdaten-RNI-1257550654-CSV
# The dataset this dataset links to (will be a list in later versions)
#LINK_DATASETS=(wikidata)

# OPTIONAL parameter(s)
# The version of the Limbo dataset
VERSION=0.0.2-SNAPSHOT

